﻿<%@ Page Title='User List' Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Default.aspx.vb" Inherits="Loan_System._Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            border-collapse: collapse;
        }
        .auto-style1, td, th {
            border: 1px solid #ddd;
            width: 200px;
            padding: 5px;
        }
        .auto-style1 th {
            background-color: #f5f5f5;
            height: 35px;
        }
            .auto-style1 td {
                padding: 5px;
            }
        .auto-style2 {
            width: 200px;
        }
        .auto-style3 {
            width: 100px;
        }
        .auto-style4 {
            width: 150px;
        }
        .auto-style5 {
            width: 16px;
        }
        #ContentPlaceHolder1_grdUser th:first-child,
        #ContentPlaceHolder1_grdUser td:first-child {
            width: 20px;
        }
        .breadcrumb li
        {
            display:inline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
        <li>
           <span style="float: right; margin-top: -4px;">
                <asp:Button ID="btnAdd" class="btn btn-info" runat="server" Text="Add" />
                <asp:Button ID="btnEdit" class="btn btn-warning" runat="server" Text="Edit" />
                <asp:Button ID="btndelete" class="btn btn-danger" runat="server" Text="Delete" />
           </span>
        </li>
    </ol>
    <asp:GridView ID="grdUser" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:CheckBox ID="chkHeader" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkCtrl" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
            <asp:BoundField DataField="Sex" HeaderText="Sex" SortExpression="Sex" />
            <asp:BoundField DataField="BOD" HeaderText="BOD" SortExpression="BOD" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
            <asp:BoundField DataField="Position" HeaderText="Position" SortExpression="Position" />
        </Columns>
</asp:GridView>
</asp:Content>
