﻿<%@ Page Title="Edit Company" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EditCompany.aspx.vb" Inherits="Loan_System.EditCompany" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            padding: 10px;
        }
        .auto-style2 {
            width: 111px;
            text-align: right;
        }
        .addcompany {
            background-color: #f5f5f5;
        }
        .btn-addcompany {
            height: 35px !important;
            width: 100px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addcompany">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">Company Name :</td>
            <td>
                <asp:TextBox ID="txtCompanyName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Website :</td>
            <td>
                <asp:TextBox ID="txtWebsite" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Phone :</td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Email :</td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Logo :</td>
            <td>
                <asp:FileUpload ID="FileUpload_Logo" Visible="true" runat="server" Height="25px" Width="218px" EnableTheming="True" />
                <asp:Label ID="lblimage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Country :</td>
            <td>
                <asp:DropDownList ID="DropDownListCountry" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td style="padding: 20px 20px 20px 0px;">
                <asp:Button ID="btnAddCompany" runat="server" CssClass="btn btn-info btn-addcompany" Text="Update" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>