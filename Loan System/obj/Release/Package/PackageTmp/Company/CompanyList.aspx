﻿<%@ Page Title="Company List" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CompanyList.aspx.vb" Inherits="Loan_System.CompanyList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        .auto-style1 {
            width: 100%;
            border-collapse: collapse;
        }
        .auto-style1, td, th {
            border: 1px solid #ddd;
            width: 200px;
            padding: 5px;
        }
        .auto-style1 th {
            background-color: #f5f5f5;
            height: 35px;
        }
            .auto-style1 td {
                padding: 5px;
            }
        .auto-style2 {
            width: 200px;
        }
        .auto-style3 {
            width: 100px;
        }
        .auto-style4 {
            width: 150px;
        }
        .auto-style5 {
            width: 16px;
        }
        #ContentPlaceHolder1_grdCompany th:first-child,
        #ContentPlaceHolder1_grdCompany td:first-child {
            width: 20px;
        }
          .breadcrumb li
        {
            display:inline;
        }
        .HiddenCol{display:none;}        
        .selected
    {
        background-color: #A1DCF2;
    }        
    </style>
    <script type="text/javascript">
        $("[id*=chkHeader]").live("click", function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    $(this).attr("checked", "checked");
                    $("td", $(this).closest("tr")).addClass("selected");
                } else {
                    $(this).removeAttr("checked");
                    $("td", $(this).closest("tr")).removeClass("selected");
                }
            });
        });
        $("[id*=chkCtrl]").live("click", function () {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            if (!$(this).is(":checked")) {
                $("td", $(this).closest("tr")).removeClass("selected");
                chkHeader.removeAttr("checked");
            } else {
                $("td", $(this).closest("tr")).addClass("selected");
                if ($("[id*=chkCtrl]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
        <li>
            <span style="float: right; margin-top: -4px;">
                <asp:Button ID="btnAdd" class="btn btn-info" runat="server" Text="Add" />
                <asp:Button ID="btnEdit" class="btn btn-warning" runat="server" Text="Edit" />
                <asp:Button ID="btndelete" class="btn btn-danger" runat="server" Text="Delete" />
            </span>
        </li>
    </ol>

    <asp:GridView ID="grdCompany" runat="server" AutoGenerateColumns="False" DataKeyNames="comCode"  >
        <Columns>
           <asp:TemplateField>
                <HeaderTemplate>
                    <asp:CheckBox ID="chkHeader" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkCtrl" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="comCode" HeaderStyle-CssClass="HiddenCol" ItemStyle-CssClass="HiddenCol" HeaderText="ID"/>
            <asp:BoundField DataField="comName" HeaderText="Company Name" SortExpression="comName" />
            <asp:BoundField DataField="website" HeaderText="Website" SortExpression="website" />
            <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
            <asp:BoundField DataField="CountryName" HeaderText="Country Name" SortExpression="CountryName" />
            <asp:ImageField DataImageUrlField = "Logo"    ControlStyle-Width = "32" ControlStyle-Height = "32" HeaderText = "Logo"/>
        </Columns>
</asp:GridView>
     </a></span>
</asp:Content>
