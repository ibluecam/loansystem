﻿Public Class AddLocation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            DropDownListFlag.Items.Add("--Select--")
            DropDownListFlag.Items.Add("Yes")
            DropDownListFlag.Items.Add("No")
        End If
    End Sub

    Protected Sub btnAddBranch_Click(sender As Object, e As EventArgs) Handles btnAddBranch.Click
        Try
            Dim locCode = CheckID("select isnull(Max(locCode),0)+1 from tblLocation")
            RunSQL("INSERT INTO tblLocation(locCode,locName,locLocalName,flag) Values(" & amStr(locCode) & "," & amStr(txtLocationName.Text) &
                   "," & amStr(txtLocalName.Text) & "," & amStr(DropDownListFlag.SelectedItem.Text) & ")")
            Response.Redirect("/Location/LocationList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class