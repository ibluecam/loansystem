﻿Public Class EditLocation
    Inherits System.Web.UI.Page
    Dim id As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            id = Request("id")
            DropDownListFlag.Items.Add("--Select--")
            DropDownListFlag.Items.Add("Yes")
            DropDownListFlag.Items.Add("No")
            Dim Location = ReadData("Select * from tblLocation where locCode=" & id)
            txtLocationName.Text = Location.Item(1)
            txtLocalName.Text = Location.Item(2)
            DropDownListFlag.Text = Location.Item(3)
        End If
    End Sub

    Protected Sub btnEditLocation_Click(sender As Object, e As EventArgs) Handles btnEditLocation.Click
        Try
            id = Request("id")
            RunSQL("UPDATE tblLocation SET locName=N" & amStr(txtLocationName.Text) & ",locLocalName=N" & amStr(txtLocalName.Text) &
                   ",flag=" & amStr(DropDownListFlag.SelectedItem.Text) & " where locCode=" & id)
            Response.Redirect("/Location/LocationList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class