﻿<%@ Page Title="Edit Location" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EditLocation.aspx.vb" Inherits="Loan_System.EditLocation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .addlocation {
            background-color: #f5f5f5;
        }
        .auto-style1 {
            padding: 10px;
            width: 100%;
        }
        .auto-style2 {
            width: 107px;
        }
        #btnBack{
            height: 18px;
            padding: 8px 12px;
            width: 65px;
            margin-left: 3px;
        }
        .btn-editlocation {
            height: 35px !important;
            margin-left: 28px;
            width: 80px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addlocation">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelLocationName" runat="server" Text="Location Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLocationName" runat="server" Height="25px" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVLocationName" runat="server" ControlToValidate="txtLocalName" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelLocalName" runat="server" Text="Local Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLocalName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelFlag" runat="server" Text="Flag :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListFlag" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" align="right">&nbsp;</td>
            <td style="padding: 20px 20px 20px 0px;">
                <a id="btnBack" class="btn btn-default" href="LocationList.aspx">Cancel</a>
                <asp:Button ID="btnEditLocation" runat="server" CssClass="btn btn-info btn-editlocation" Text="Update" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>
