﻿Public Class EditBranch
    Inherits System.Web.UI.Page
    Dim id As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            id = Request("id")
            DpdIsBranch.Items.Add("--Select--")
            DpdIsBranch.Items.Add("Yes")
            DpdIsBranch.Items.Add("No")
            DpdFlag.Items.Add("--Select--")
            DpdFlag.Items.Add("Yes")
            DpdFlag.Items.Add("No")
            addToDropdown("Select comCode,comName From tblCompany ", DpdCompany)
            Dim Branch = ReadData(" SELECT [branchCode],[branchName],[branchShortName],[branchLocalName],[locCode],[branchAdd] " & _
                       ",[Isbranch],[SubBranchof],[email],[phone],[website],[comCode],[flag],[POBox] FROM tblBranch Where branchCode=" & id)
            txtBranchName.Text = Branch.Item(1)
            txtBranchShortName.Text = Branch.Item(2)
            txtLocalName.Text = Branch.Item(3)
            DpdLocation.Text = Branch.Item(4)
            txtBranchAddress.Text = Branch.Item(5)
            DpdIsBranch.Text = Branch.Item(6)
            txtSubBranch.Text = Branch.Item(7)
            txtEmail.Text = Branch.Item(8)
            txtPhone.Text = Branch.Item(9)
            txtWebsite.Text = Branch.Item(10)
            DpdCompany.Text = Branch.Item(11)
            DpdFlag.Text = Branch.Item(12)
            txtPoBox.Text = Branch.Item(13)
        End If
    End Sub

    Protected Sub btnAddBranch_Click(sender As Object, e As EventArgs) Handles btnAddBranch.Click
        Try
            id = Request("id")
            RunSQL("UPDATE tblBranch SET branchName=N" & amStr(txtBranchName.Text) & ",branchShortName=N" & amStr(txtBranchShortName.Text) &
                   ",branchLocalName=N" & amStr(txtLocalName.Text) & ",locCode=" & amStr(DpdLocation.SelectedValue) & ",branchAdd=N" & amStr(txtBranchAddress.Text) &
                   ",Isbranch=" & amStr(DpdIsBranch.SelectedItem.Text) & ",subBranchof=N" & amStr(txtSubBranch.Text) & ",email=" & amStr(txtEmail.Text) & ",phone=" & amStr(txtPhone.Text) &
                   ",website=" & amStr(txtWebsite.Text) & ",comCode=" & amStr(DpdCompany.SelectedValue) & ",flag=" & amStr(DpdFlag.SelectedItem.Text) & ",POBox=N" & amStr(txtPoBox.Text) & " Where branchCode=" & id)
            Response.Redirect("/Branch/BranchList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class