﻿<%@ Page Title="Add Branch" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddBranch.aspx.vb" Inherits="Loan_System.AddBranch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .addbranch {
            background-color: #f5f5f5;
        }
        .auto-style1 {
            padding: 10px;
            width: 70%;
        }
        .auto-style3 {
            width: 240px;
        }
        .auto-style5 {
            width: 95px;
        }
        .auto-style6 {
            width: 87px;
        }
        #btnBack{
            height: 18px;
            padding: 8px 12px;
            width: 65px;
            margin-left: 3px;
        }
        .btn-addbranch {
            height: 35px !important;
            margin-left: 28px;
            width: 80px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addbranch">
    <table class="auto-style1">
        <tr>
            <td class="auto-style5" align="right">
                <asp:Label ID="LabelBranch" runat="server" Text="Branch Name :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtBranchName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style6" align="right">
                <asp:Label ID="LabelShortName" runat="server" Text="Short Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtBranchShortName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style5" align="right">
                <asp:Label ID="LabelLocalName" runat="server" Text="Local Name :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtLocalName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style6" align="right">
                <asp:Label ID="LabelLocation" runat="server" Text="Location :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListLocation" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style5" align="right">
                <asp:Label ID="LabelAddress" runat="server" Text="Address :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtBranchAddress" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style6" align="right">
                <asp:Label ID="LabelSubbranch" runat="server" Text="Sub Branch :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtSubBranch" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style5" align="right">
                <asp:Label ID="LabelWebsite" runat="server" Text="Website :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtWebsite" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style6" align="right">
                <asp:Label ID="LabelEmail" runat="server" Text="Email :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style5" align="right">
                <asp:Label ID="LabelPhone" runat="server" Text="Phone :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtPhone" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style6" align="right">
                <asp:Label ID="LabelPOBox" runat="server" Text="POBox :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPoBox" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style5" align="right">
                <asp:Label ID="LabelIsBranch" runat="server" Text="IsBranch :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:DropDownList ID="DpdIsBranch" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
            <td class="auto-style6" align="right">
                <asp:Label ID="LabelFlag" runat="server" Text="Flag :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListFlag" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style5" align="right">
                <asp:Label ID="LabelCompany" runat="server" Text="Company :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:DropDownList ID="DdpCompany" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
            <td class="auto-style6" align="right">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style7">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style11" style="padding: 20px 20px 20px 0px;">
                <a id="btnBack" class="btn btn-default" href="BranchList.aspx">Back</a>
                <asp:Button ID="btnAddBranch" runat="server" CssClass="btn btn-info btn-addbranch" Text="Add" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>
