﻿Public Class AddBranch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            DpdIsBranch.Items.Add("--Select--")
            DpdIsBranch.Items.Add("Yes")
            DpdIsBranch.Items.Add("No")
            DropDownListFlag.Items.Add("--Select--")
            DropDownListFlag.Items.Add("Yes")
            DropDownListFlag.Items.Add("No")
            addToDropdown("Select comCode,comName From tblCompany ", DdpCompany)
        End If
    End Sub

    Protected Sub btnAddBranch_Click(sender As Object, e As EventArgs) Handles btnAddBranch.Click
        Try
            Dim branchCode = CheckID("select isnull(Max(branchCode),0)+1 from tblBranch")
            RunSQL("INSERT INTO tblBranch(branchCode,branchName,branchShortName,branchLocalName,locCode,branchAdd,Isbranch,SubBranchof,email,phone,website,flag,POBox,comCode) " & _
                   " Values(" & amStr(branchCode) & "," & amStr(txtBranchName.Text) & "," & amStr(txtBranchShortName.Text) & "," & amStr(txtLocalName.Text) &
                   "," & amStr(DropDownListLocation.SelectedValue) & "," & amStr(txtBranchAddress.Text) & "," & amStr(DpdIsBranch.SelectedItem.Text) &
                   "," & amStr(txtSubBranch.Text) & "," & amStr(txtEmail.Text) & "," & amStr(txtPhone.Text) & "," & amStr(txtWebsite.Text) &
                   "," & amStr(DropDownListFlag.SelectedItem.Text) & "," & amStr(txtPoBox.Text) & "," & amStr(DdpCompany.Text) & " ) ")
            Response.Redirect("/Branch/BranchList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class