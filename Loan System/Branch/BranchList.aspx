﻿<%@ Page Title="Branch List" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="BranchList.aspx.vb" Inherits="Loan_System.BranchList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
          <style type="text/css">
        #ContentPlaceHolder1_grdBranch {
            width: 100%;
            border-collapse: collapse;
        }
        .auto-style1, td, th {
            border: 1px solid #ddd;
            width: 200px;
            padding: 5px;
        }
        .auto-style1 th {
            background-color: #f5f5f5;
            height: 35px;
        }
            .auto-style1 td {
                padding: 5px;
            }
        .auto-style2 {
            width: 200px;
        }
        .auto-style3 {
            width: 100px;
        }
        .auto-style4 {
            width: 150px;
        }
        .auto-style5 {
            width: 16px;
        }
        #ContentPlaceHolder1_grdBranch th:first-child,
        #ContentPlaceHolder1_grdBranch td:first-child {
            width: 20px;
        }
          .breadcrumb li
        {
            display:inline;
        }
        .HiddenCol{display:none;}        
        .selected
    {
        background-color: #A1DCF2;
    }
        /*Search*/
        #tfheader{
		   margin-bottom: 10px;
	    }
	    #tfnewsearch{
		    float:right;
		    padding:20px;
	    }
	    .tftextinput{
		    margin: 0;
		    padding: 5px 15px;
		    font-family: Arial, Helvetica, sans-serif;
		    font-size:14px;
		    border:1px solid #0076a3; border-right:0px;
		    border-top-left-radius: 5px 5px;
		    border-bottom-left-radius: 5px 5px;
	    }
	    .tfbutton {
		    margin: 0;
		    padding: 5px 15px;
		    font-family: Arial, Helvetica, sans-serif;
		    font-size:14px;
		    outline: none;
		    cursor: pointer;
		    text-align: center;
		    text-decoration: none;
		    color: #ffffff;
		    border: solid 1px #0076a3; border-right:0px;
		    background: #0095cd;
		    background: -webkit-gradient(linear, left top, left bottom, from(#00adee), to(#0078a5));
		    background: -moz-linear-gradient(top,  #00adee,  #0078a5);
		    border-top-right-radius: 5px 5px;
		    border-bottom-right-radius: 5px 5px;
	    }
	    .tfbutton:hover {
		    text-decoration: none;
		    background: #007ead;
		    background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
		    background: -moz-linear-gradient(top,  #0095cc,  #00678e);
	    }
	    /* Fixes submit button height problem in Firefox */
	    .tfbutton::-moz-focus-inner {
	      border: 0;
	    }
	    .tfclear{
		    clear:both;
	    }
    </style>
    <script type="text/javascript">
        $("[id*=chkHeader]").live("click", function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    $(this).attr("checked", "checked");
                    $("td", $(this).closest("tr")).addClass("selected");
                } else {
                    $(this).removeAttr("checked");
                    $("td", $(this).closest("tr")).removeClass("selected");
                }
            });
        });
        $("[id*=chkCtrl]").live("click", function () {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            if (!$(this).is(":checked")) {
                $("td", $(this).closest("tr")).removeClass("selected");
                chkHeader.removeAttr("checked");
            } else {
                $("td", $(this).closest("tr")).addClass("selected");
                if ($("[id*=chkCtrl]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
        <li>
            <span style="float: right; margin-top: -4px;">
                <asp:Button ID="btnAdd" class="btn btn-info" runat="server" Text="Add" />
                <asp:Button ID="btnEdit" class="btn btn-warning" runat="server" Text="Edit" />
                <asp:Button ID="btndelete" class="btn btn-danger" runat="server" Text="Delete" />
            </span>
        </li>
    </ol>
    <div id="tfheader">
		<form id="tfnewsearch" action="#">
		    <input type="text" class="tftextinput" name="q" size="21" maxlength="120"/><input type="submit" value="search" class="tfbutton"/>
		</form>
		<div class="tfclear"></div>
	</div>
    <asp:GridView ID="grdBranch" runat="server" EmptyDataText = "No Records Found" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="comCode"  >
        <Columns>
           <asp:TemplateField>
                <HeaderTemplate>
                    <asp:CheckBox ID="chkHeader" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkCtrl" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="branchCode" HeaderStyle-CssClass="HiddenCol" ItemStyle-CssClass="HiddenCol" HeaderText="ID"/>
            <asp:BoundField DataField="branchName" HeaderText="Name" SortExpression="branchName" />
            <asp:BoundField DataField="branchShortName" HeaderText="Short Name" SortExpression="branchShortName" />
            <asp:BoundField DataField="branchLocalName" HeaderText="Local Name" SortExpression="branchLocalName" />
            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
            <asp:BoundField DataField="phone" HeaderText="phone" SortExpression="phone" />
         
        </Columns>
    </asp:GridView>
</asp:Content>
