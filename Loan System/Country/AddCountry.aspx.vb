﻿Public Class AddCountry
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            DpdFlag.Items.Add("--Select--")
            DpdFlag.Items.Add("Yes")
            DpdFlag.Items.Add("No")
        End If
    End Sub

    Protected Sub btnAddCountry_Click(sender As Object, e As EventArgs) Handles btnAddCountry.Click
        Try
            Dim CountryCode = CheckID("select isnull(Max(CountryCode),0)+1 from tblCountry")
            RunSQL("INSERT INTO tblCountry(countryCode,CountryName,CountryShortName,counLanguage,ccyCode,ccyName,flag) " & _
                   " Values(" & amStr(CountryCode) & "," & amStr(txtCountryName.Text) & "," & amStr(txtCountryShortName.Text) &
                   ", " & amStr(txtLanguage.Text) & "," & amStr(txtccyCode.Text) & "," & amStr(txtccyName.Text) & "," & amStr(DpdFlag.SelectedItem.Text) & ")")
            Response.Redirect("/Country/CountryList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class