﻿Public Class EditCountry
    Inherits System.Web.UI.Page
    Dim id As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            id = Request("id")
            DropDownListFlag.Items.Add("--Select--")
            DropDownListFlag.Items.Add("Yes")
            DropDownListFlag.Items.Add("No")
            Dim Country = ReadData("Select CountryCode,CountryName,CountryShortName,counLanguage,ccyCode,ccyName,flag From tblCountry Where CountryCode=" & id)
            txtCountryName.Text = Country.Item(1)
            txtCountryShortName.Text = Country.Item(2)
            txtLanguage.Text = Country.Item(3)
            txtccyCode.Text = Country.Item(4)
            txtccyName.Text = Country.Item(5)
            DropDownListFlag.Text = Country.Item(6)
        End If
    End Sub

    Protected Sub btnEditCountry_Click(sender As Object, e As EventArgs) Handles btnEditCountry.Click
        Try
            id = Request("id")
            RunSQL("UPDATE tblCountry SET CountryName=N" & amStr(txtCountryName.Text) & ",CountryShortName=N" & amStr(txtCountryShortName.Text) &
                   ",CounLanguage=N" & amStr(txtLanguage.Text) & ",ccyCode=N" & amStr(txtccyCode.Text) & ",ccyName=N" & amStr(txtccyName.Text) &
                   ",flag=" & amStr(DropDownListFlag.SelectedItem.Text) & " Where CountryCode=" & id)
            Response.Redirect("/Country/CountryList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class