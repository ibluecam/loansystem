﻿Public Class CountryList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadCountry()
        End If

    End Sub
    Sub LoadCountry()
        FillToGridView("Select CountryCode,CountryName,CountryShortName,counLanguage,ccyCode,ccyName,flag From tblCountry Where CountryStatus IS NULL", grdCountry)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("/Country/AddCountry.aspx")
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            Dim count As Integer = 0
            If CheckboxIsSelected(grdCountry) Then
                For Each R As GridViewRow In grdCountry.Rows
                    Dim chkRow As CheckBox = TryCast(R.Cells(0).FindControl("chkCtrl"), CheckBox)
                    If chkRow.Checked Then
                        count += 1
                    End If
                Next
                If count > 1 Then
                    MsgBox("You can select only for edit", MsgBoxStyle.Critical)
                Else
                    For Each row As GridViewRow In grdCountry.Rows
                        Dim chkRow As CheckBox = TryCast(row.Cells(0).FindControl("chkCtrl"), CheckBox)
                        If chkRow.Checked Then
                            Response.Redirect("/Country/EditCountry.aspx?id=" & row.Cells(1).Text, False)
                        End If
                    Next
                End If
            Else
                MsgBox("Please Check to Edit!", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs) Handles btndelete.Click
        Try
            If CheckboxIsSelected(grdCountry) Then
                If MsgBox("Do you want to delete record(s)", MsgBoxStyle.YesNo, "Confirm Delete") = MsgBoxResult.Yes Then
                    For Each R As GridViewRow In grdCountry.Rows
                        Dim chkRow As CheckBox = TryCast(R.Cells(0).FindControl("chkCtrl"), CheckBox)
                        If chkRow.Checked Then
                            RunSQL("UPDATE tblCountry SET CountryStatus='Deleted' where CountryCode=" & R.Cells(1).Text)
                        End If
                    Next
                    Response.Redirect("/Country/CountryList.aspx", False)
                End If
            Else
                MsgBox("Please Check to Delete!", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class