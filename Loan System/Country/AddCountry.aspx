﻿<%@ Page Title="Add Country" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddCountry.aspx.vb" Inherits="Loan_System.AddCountry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .addcountry {
            background-color: #f5f5f5;
        }
        .auto-style1 {
            padding: 10px;
            width: 70%;
        }
        .auto-style2 {
            width: 100px;
        }
        .auto-style3 {
            width: 234px;
        }
        #btnBack{
            height: 18px;
            padding: 8px 12px;
            width: 65px;
            margin-left: 3px;
        }
        .btn-addcountry {
            height: 35px !important;
            margin-left: 30px;
            width: 80px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addcountry">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelCountryName" runat="server" Text="Country Name :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtCountryName" runat="server" Height="25px" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVCountryName" runat="server" ControlToValidate="txtCountryName" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelCountryShortName" runat="server" Text="Short Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCountryShortName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelLanguage" runat="server" Text="Language :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtLanguage" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelFlag" runat="server" Text="Flag :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DpdFlag" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelccyCode" runat="server" Text="ccyCode :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtccyCode" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style2" align="right">
                <asp:Label ID="LabelccyName" runat="server" Text="ccyName :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtccyName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" align="right">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style2" align="right">&nbsp;</td>
            <td style="padding: 20px 20px 20px 0px;">
                <a id="btnBack" class="btn btn-default" href="CountryList.aspx">Back</a>
                <asp:Button ID="btnAddCountry" runat="server" CssClass="btn btn-info btn-addcountry" Text="Add" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>
