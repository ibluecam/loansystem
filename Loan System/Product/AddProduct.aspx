﻿<%@ Page Title="Add Product" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddProduct.aspx.vb" Inherits="Loan_System.AddProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .addproduct {
            background-color: #f5f5f5;
        }
        .auto-style1 {
            padding: 10px;
            width: 100%;
        }
        .auto-style2 {
            width: 111px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addproduct">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lbProductName" runat="server" Text="Product Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtProductName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbShortName" runat="server" Text="Short Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtShortName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lbLoanFee" runat="server" Text="Loan Fee :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLoanFee" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbLoanFeeRate" runat="server" Text="Loan Fee Rate :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLoanFeeRate" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lbServiceCharge" runat="server" Text="Service Charge :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtServiceCharge" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbInterestRate" runat="server" Text="Interest Rate :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtInterestRate" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
    </table>
</div>
</asp:Content>
