﻿Public Class ProductList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadProduct()
        End If
    End Sub
    Sub LoadProduct()
        FillToGridView(" SELECT * FROM tblProduct ", grdProduct)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("/Product/AddProduct.aspx")
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            Dim count As Integer = 0
            If CheckboxIsSelected(grdProduct) Then
                For Each R As GridViewRow In grdProduct.Rows
                    Dim chkRow As CheckBox = TryCast(R.Cells(0).FindControl("chkCtrl"), CheckBox)
                    If chkRow.Checked Then
                        count += 1
                    End If
                Next
                If count > 1 Then
                    MsgBox("You can select only for edit", MsgBoxStyle.Critical)
                Else
                    For Each row As GridViewRow In grdProduct.Rows
                        Dim chkRow As CheckBox = TryCast(row.Cells(0).FindControl("chkCtrl"), CheckBox)
                        If chkRow.Checked Then
                            Response.Redirect("/Product/EditProduct.aspx?id=" & row.Cells(1).Text, False)
                        End If
                    Next
                End If
            Else
                MsgBox("Please Check to Edit!", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class