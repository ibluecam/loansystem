﻿<%@ Page Title="Product List" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ProductList.aspx.vb" Inherits="Loan_System.ProductList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #ContentPlaceHolder1_grdProduct {
            width: 100%;
            border-collapse: collapse;
        }
        #ContentPlaceHolder1_grdProduct th:first-child,
        #ContentPlaceHolder1_grdProduct td:first-child {
            width: 20px;
        }
        #ContentPlaceHolder1_grdProduct, td, th {
            border: 1px solid #ddd;
            padding: 5px;
        }
        .breadcrumb li
        {
            display:inline;
        }
        .HiddenCol{display:none;}        
        .selected {
            background-color: #A1DCF2;
        }
        /*Search*/
        #tfheader{
		   margin-bottom: 10px;
	    }
	    #tfnewsearch{
		    float:right;
		    padding:20px;
	    }
	    .tftextinput{
		    margin: 0;
		    padding: 5px 15px;
		    font-family: Arial, Helvetica, sans-serif;
		    font-size:14px;
		    border:1px solid #0076a3; border-right:0px;
		    border-top-left-radius: 5px 5px;
		    border-bottom-left-radius: 5px 5px;
	    }
	    .tfbutton {
		    margin: 0;
		    padding: 5px 15px;
		    font-family: Arial, Helvetica, sans-serif;
		    font-size:14px;
		    outline: none;
		    cursor: pointer;
		    text-align: center;
		    text-decoration: none;
		    color: #ffffff;
		    border: solid 1px #0076a3; border-right:0px;
		    background: #0095cd;
		    background: -webkit-gradient(linear, left top, left bottom, from(#00adee), to(#0078a5));
		    background: -moz-linear-gradient(top,  #00adee,  #0078a5);
		    border-top-right-radius: 5px 5px;
		    border-bottom-right-radius: 5px 5px;
	    }
	    .tfbutton:hover {
		    text-decoration: none;
		    background: #007ead;
		    background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
		    background: -moz-linear-gradient(top,  #0095cc,  #00678e);
	    }
	    /* Fixes submit button height problem in Firefox */
	    .tfbutton::-moz-focus-inner {
	        border: 0;
	    }
	    .tfclear{
		    clear:both;
	    }
    </style>
    <script type="text/javascript">
        $("[id*=chkHeader]").live("click", function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    $(this).attr("checked", "checked");
                    $("td", $(this).closest("tr")).addClass("selected");
                } else {
                    $(this).removeAttr("checked");
                    $("td", $(this).closest("tr")).removeClass("selected");
                }
            });
        });
        $("[id*=chkCtrl]").live("click", function () {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            if (!$(this).is(":checked")) {
                $("td", $(this).closest("tr")).removeClass("selected");
                chkHeader.removeAttr("checked");
            } else {
                $("td", $(this).closest("tr")).addClass("selected");
                if ($("[id*=chkCtrl]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
        <li>
            <span style="float: right; margin-top: -4px;">
                <asp:Button ID="btnAdd" CssClass="btn btn-info" runat="server" Text="Add" />
                <asp:Button ID="btnEdit" CssClass="btn btn-warning" runat="server" Text="Edit" />
                <asp:Button ID="btndelete" CssClass="btn btn-danger" runat="server" Text="Delete" />
            </span>
        </li>
    </ol>
    <div id="tfheader">
		<form id="tfnewsearch" action="#">
		    <input type="text" class="tftextinput" name="q" size="21" maxlength="120"/><input type="submit" value="search" class="tfbutton"/>
		</form>
		<div class="tfclear"></div>
	</div>
    <asp:GridView ID="grdProduct" runat="server" EmptyDataText = "No Records Found" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="ProCode"  >
        <Columns>
           <asp:TemplateField>
                <HeaderTemplate>
                    <asp:CheckBox ID="chkHeader" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkCtrl" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProCode" HeaderStyle-CssClass="HiddenCol" ItemStyle-CssClass="HiddenCol" HeaderText="ID"/>
            <asp:BoundField DataField="ProName" HeaderText="Product Name" SortExpression="ProName" />
            <asp:BoundField DataField="ProShortName" HeaderText="Short Name" SortExpression="ProShortName" />
            <asp:BoundField DataField="LoanFee" HeaderText="Loan Fee" SortExpression="LoanFee" />
            <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge" SortExpression="ServiceCharge" />
            <asp:BoundField DataField="LoanFreeRate" HeaderText="Loan Free Rate" SortExpression="LoanFreeRate" />
            <asp:BoundField DataField="InterestRate" HeaderText="Interest Rate" SortExpression="InterestRate" />
            <asp:BoundField DataField="PenaltyRate" HeaderText="Penalty Rate" SortExpression="PenaltyRate" />
            <asp:BoundField DataField="PenaltyScheme" HeaderText="Penalty Scheme" SortExpression="PenaltyScheme" />
            <asp:BoundField DataField="CivilStatus" HeaderText="Civil Status" SortExpression="CivilStatus" />
            <asp:BoundField DataField="DayDivisor" HeaderText="Day Divisor" SortExpression="DayDivisor" />
            <asp:BoundField DataField="LimitAmt" HeaderText="Limit Amt" SortExpression="LimitAmt" />
            <asp:BoundField DataField="MinSize" HeaderText="Min Size" SortExpression="MinSize" />
            <asp:BoundField DataField="MaxSize" HeaderText="Max Size" SortExpression="MaxSize" />
            <asp:BoundField DataField="MinTerm" HeaderText="Min Term" SortExpression="MinTerm" />
            <asp:BoundField DataField="MaxTerm" HeaderText="Max Term" SortExpression="MaxTerm" />
            <asp:BoundField DataField="CurrencyCode" HeaderText="Currency Code" SortExpression="CurrencyCode" />
            <asp:BoundField DataField="LoanTypeCode" HeaderText="Loan Type" SortExpression="LoanTypeCode" />
            <asp:BoundField DataField="MinimumBalance" HeaderText="Min Balance" SortExpression="MinimumBalance" />
            <asp:BoundField DataField="AccrBalance" HeaderText="Accr Balance" SortExpression="AccrBalance" />
            <asp:BoundField DataField="ProType" HeaderText="Product Type" SortExpression="ProType" />
            <asp:BoundField DataField="Tax" HeaderText="Tax" SortExpression="Tax" />
        </Columns>
    </asp:GridView>
</asp:Content>
