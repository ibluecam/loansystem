﻿Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadUser()
        End If
    End Sub

    Sub LoadUser()
        FillToGridView("SELECT [UserName], [Sex], CONVERT(varchar,BOD,101) as BOD, [Email], [Phone], [Position] FROM [tblUser]", grdUser)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("Register.aspx")
    End Sub
End Class