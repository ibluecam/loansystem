﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Security.Cryptography
Module encrypt_decrypt

    Dim KEY_128 As Byte() = {42, 1, 52, 67, 231, 13, 94, 101, 123, 6, 0, 12, 32, 91, 4, 111, 31, 70, 21, 141, 123, 142, 234, 82, 95, 129, 187, 162, 12, 55, 98, 23}
    Dim IV_128 As Byte() = {234, 12, 52, 44, 214, 222, 200, 109, 2, 98, 45, 76, 88, 53, 23, 78}
    Dim symmetricKey As New RijndaelManaged
    Dim enc As System.Text.UTF8Encoding = New System.Text.UTF8Encoding
    Dim encryptor As ICryptoTransform = symmetricKey.CreateEncryptor(KEY_128, IV_128)
    Dim decryptor As ICryptoTransform = symmetricKey.CreateDecryptor(KEY_128, IV_128)

    Public Function EncryptStr(ByVal str As String) As String
        Dim memoryStream As MemoryStream = New MemoryStream()
        Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
        cryptoStream.Write(enc.GetBytes(str), 0, str.Length)
        cryptoStream.FlushFinalBlock()
        Return Convert.ToBase64String(memoryStream.ToArray())
    End Function
    Public Function DecryptStr(ByVal str As String) As String
        Dim cypherTextBytes As Byte() = Convert.FromBase64String(str)
        Dim memoryStream As MemoryStream = New MemoryStream(cypherTextBytes)
        Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
        Dim plainTextBytes(cypherTextBytes.Length) As Byte
        Dim decryptedByteCount As Integer = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length)
        Return enc.GetString(plainTextBytes, 0, decryptedByteCount)
    End Function

End Module
