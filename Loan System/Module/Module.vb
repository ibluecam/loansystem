﻿Imports System.Web
Imports System.Data
Imports System.Linq
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports System.IO
Imports System.Collections.Generic

Module MyModule
    Public CN As SqlConnection
    Public LoginID As String = "", LoginName As String = "", LoginFullName As String = ""
    Public Sub makeConnection()
        CN = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnString").ConnectionString)
        CN.Open()
    End Sub
    Public Sub RunSQL(ByVal SQL As String, Optional ByVal t As SqlTransaction = Nothing)
        Dim cmd As SqlCommand
        If Not IsNothing(t) Then
            cmd = New SqlCommand(SQL, CN, t)
        Else
            cmd = New SqlCommand(SQL, CN)
        End If
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        cmd = Nothing
    End Sub
    Public Sub RunSQLWithImage(ByVal SQL As String, ByVal imgUpload As FileUpload, Optional ByVal t As SqlClient.SqlTransaction = Nothing)
        Dim cmd As SqlClient.SqlCommand
        If Not IsNothing(t) Then
            cmd = New SqlClient.SqlCommand(SQL, CN, t)
        Else
            cmd = New SqlClient.SqlCommand(SQL, CN)
        End If
        If imgUpload.HasFile Then
            Dim filename As String = Path.GetFileName(imgUpload.PostedFile.FileName)
            imgUpload.SaveAs(HttpContext.Current.Server.MapPath("/images/" & filename))
            Dim p As New SqlParameter("@image", SqlDbType.NVarChar)
            p.Value = "/images/" & filename
            cmd.Parameters.Add(p)
        Else
            Dim p As New SqlParameter("@image", SqlDbType.NVarChar)
            p.Value = ""
            cmd.Parameters.Add(p)
        End If
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        cmd = Nothing
    End Sub
    Public Function amStr(ByVal S As String) As String
        S = Replace(S, "'", "''")
        Return "'" & S & "'"
    End Function

    Public Sub addToDropdown(ByVal SQL As String, ByVal CB As Object)
        Dim adpt As New SqlDataAdapter(SQL, CN)
        Dim dt = New DataTable()
        adpt.Fill(dt)
        CB.DataSource = dt
        CB.DataTextField = dt.Columns(1).ColumnName
        CB.DataValueField = dt.Columns(0).ColumnName
        CB.DataBind()
        CB.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Public Function getValue(ByVal SQL As String, Optional ByVal t As SqlTransaction = Nothing) As String
        Dim data As String = ""
        Dim cmd As SqlCommand
        If Not IsNothing(t) Then
            cmd = New SqlCommand(SQL, CN, t)
        Else
            cmd = New SqlCommand(SQL, CN)
        End If
        If Not IsDBNull(cmd.ExecuteScalar) Then
            data = cmd.ExecuteScalar
        End If
        cmd.Dispose()
        cmd = Nothing
        Return data
    End Function

    Public Sub FillToGridView(ByVal SQL As String, ByVal grd As GridView)
        Dim cmd As SqlCommand
        Dim ds As DataSet
        Dim dpt As SqlDataAdapter
        cmd = New SqlCommand(SQL, CN)
        ds = New DataSet
        dpt = New SqlDataAdapter(cmd)
        dpt.Fill(ds)
        grd.DataSource = ds
        grd.DataBind()
        cmd.Dispose()
        cmd = Nothing
    End Sub

    Public Function CountRecord(ByVal grd As GridView) As String
        Dim str As String
        Dim _TotalRecs As Integer = grd.Rows.Count
        Dim _CurrentRecStart As Integer = grd.PageIndex * grd.PageSize + 1
        Dim _CurrentRecEnd As Integer = grd.PageIndex * grd.PageSize + grd.Rows.Count
        str = String.Format("Displaying {0} of {1} records found", _TotalRecs, _CurrentRecEnd)
        Return str
    End Function

    Public Function CheckID(ByVal SQL As String) As Integer
        Dim id As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand(SQL, CN)
        id = cmd.ExecuteScalar
        Return id
    End Function

    Public Function ReadData(ByVal SQL As String) As List(Of String)
        Dim data As New List(Of String)()
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        cmd = New SqlCommand(SQL, CN)
        dr = cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read()
                For i As Integer = 0 To dr.FieldCount - 1
                    data.Add(dr(i).ToString)
                Next
            End While
        End If
        cmd.Dispose()
        cmd = Nothing
        dr.Close()
        Return data
    End Function

    Function CheckboxIsSelected(ByVal grd As GridView) As Boolean
        For Each R As GridViewRow In grd.Rows
            Dim chkRow As CheckBox = TryCast(R.Cells(0).FindControl("chkCtrl"), CheckBox)
            If Convert.ToBoolean(chkRow.Checked) Then
                Return True
            End If
        Next
        Return False
    End Function
End Module
