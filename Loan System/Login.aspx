﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="Loan_System.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Log In</title>
    <style type="text/css">
        body {
            font-family: Arial, Verdana;
            color: #333333;
            background-image: url(../images/bg.jpg);
            background-position: center;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .page {
            background: #fff none repeat scroll 0 0;
            border: 5px solid #ddd;
            border-radius: 5px;
            box-shadow: 1px 1px 2px 2px;
            margin: 110px auto;
            padding: 22px;
            text-align: center;
            width: 530px;
        }
        .auto-style1 {
            border: 1px solid #ddd;
            border-radius: 5px;
            padding: 5px;
            width: 310px;
        }
        .auto-style2 {
            text-align: right;
            width: 569px;
        }
            .auto-style2 strong {
                font-size: 13px;
            }
        .auto-style3 {
            text-align: left;
            width: 318px;
        }
            .auto-style3 #ImageLogin {
                width: 25px;
                height: auto;
            }    
            .auto-style3 h1 {
                margin: 0px;
            }
            .auto-style3 #btnLogIn {
                background-color: #4caf50;
                border: medium none;
                border-radius: 5px;
                color: white;
                cursor: pointer;
                display: inline-block;
                font-size: 14px;
                margin: 4px 2px;
                padding: 5px;
                text-align: center;
                text-decoration: none;
                font-family: arial,verdana;
            }
        .auto-style4 #tblStatus {
            padding-left: 10px;
        }
               /*.auto-style5 #msgUserName,
            .auto-style5 #msgPassword {
                padding-left: 22px;
            }*/
        .left {
            float: left;
            width: 40%;
            height: 265px;
        }
            .left #logo {
                height: auto;
                max-height: 100%;
                max-width: 100%;
                width: auto;
            }
        .right {
            width: 59%;
            float: right;
            margin-left: 0px;
        }
        .clear {
            clear: both;
        }
        .auto-style4 {
            text-align: left;
            }
    </style>
    </head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <%--Framework scripts--%>
            
            
   
            <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/js/WebForms/WebForms.js" />
            <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/js/WebForms/WebUIValidation.js" />
            <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/js/WebForms/MenuStandards.js" />
            <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/js/WebForms/GridView.js" />
            <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/js/WebForms/DetailsView.js" />
            <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/js/WebForms/TreeView.js" />
            <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/js/WebForms/WebParts.js" />
            <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/js/WebForms/Focus.js" />
            
            <%--Site scripts--%>

        </Scripts>
    </asp:ScriptManager>
        <div class="page">
            <div class="left">
                <asp:Image ID="logo" runat="server" ImageUrl="~/images/loan.jpg" />
            </div>
            <div class="right">
                <table class="auto-style1">
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3"><h1>
                    <asp:Image ID="ImageLogin" runat="server" ImageUrl="~/images/login.png" />
                    Log In</h1></td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td class="auto-style3">
                    <asp:RequiredFieldValidator ID="msgUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="User Name Required" ForeColor="#CC0000" Font-Size="Small"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2"><strong>Username :</strong></td>
                <td class="auto-style3">
                    <asp:TextBox ID="txtUserName" runat="server" Width="200px" Height="25px" style="text-align: left"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td class="auto-style3">
                    <asp:RequiredFieldValidator ID="msgPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password Required" ForeColor="#CC0000" Font-Size="Small"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2"><strong>Password :</strong></td>
                <td class="auto-style3">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="200px" Height="25px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4" colspan="2">
                    <asp:Label ID="tblStatus" runat="server" Font-Size="Small" ForeColor="#CC0000"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">&nbsp;</td>
                <td class="auto-style3">
                    <asp:CheckBox ID="RememberMe" runat="server" Font-Size="Small" Text="Remember me" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3">
                    <asp:Button ID="btnLogIn" runat="server" Text="Log In" Width="100px" Font-Bold="True" />
                </td>
            </tr>
        </table>
            </div>
            <div class="clear"></div>
        </div>
    </form>
</body>
</html>
