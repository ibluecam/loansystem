﻿Imports System.Drawing

Public Class CompanyList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadCompany()
        End If
    End Sub

    Sub LoadCompany()
        FillToGridView("Select comCode,comName,website,phone,email,CountryName,Logo From tblCompany Left JOIN tblCountry ON tblCountry.CountryCode=tblCompany.countryCode Where tblCompany.comStatus IS NULL", grdCompany)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("/Company/Addcompany.aspx")
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim count As Integer = 0
        If CheckboxIsSelected(grdCompany) Then
            For Each R As GridViewRow In grdCompany.Rows
                Dim chkRow As CheckBox = TryCast(R.Cells(0).FindControl("chkCtrl"), CheckBox)
                If chkRow.Checked Then
                    count += 1
                End If
            Next
            If count > 1 Then
                MsgBox("You can select only for edit", MsgBoxStyle.Critical)
            Else
                For Each row As GridViewRow In grdCompany.Rows
                    Dim chkRow As CheckBox = TryCast(row.Cells(0).FindControl("chkCtrl"), CheckBox)
                    If chkRow.Checked Then
                        Response.Redirect("/Company/EditCompany.aspx?id=" & row.Cells(1).Text)
                    End If
                Next
            End If
        Else
            MsgBox("Please Check Company to Edit!", MsgBoxStyle.Critical)
        End If

    End Sub


    Protected Sub btndelete_Click(sender As Object, e As EventArgs) Handles btndelete.Click
        If CheckboxIsSelected(grdCompany) Then
            If MsgBox("Do you want to delete record(s)", MsgBoxStyle.YesNo, "Confirm Delete") = MsgBoxResult.Yes Then
                For Each R As GridViewRow In grdCompany.Rows
                    Dim chkRow As CheckBox = TryCast(R.Cells(0).FindControl("chkCtrl"), CheckBox)
                    If chkRow.Checked Then
                        RunSQL("UPDATE tblCompany SET comStatus='Deleted' where comCode=" & R.Cells(1).Text)
                    End If
                Next
                Response.Redirect("/Company/CompanyList.aspx", False)
            End If
        Else
            MsgBox("Please Check Company to Delete!", MsgBoxStyle.Critical)
        End If
    End Sub
End Class
