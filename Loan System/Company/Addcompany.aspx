﻿<%@ Page Title="Add Company" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Addcompany.aspx.vb" Inherits="Loan_System.Addcompany" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 75%;
            padding: 10px;
        }
        .auto-style2 {
            width: 104px;
            text-align: right;
        }
        .addcompany {
            background-color: #f5f5f5;
        }
        .btn-addcompany {
            float: right;
            height: 35px !important;
            margin-right: 30px;
            width: 80px !important;
        }
        .auto-style3 {
            width: 104px;
            text-align: right;
            height: 38px;
        }
        .auto-style5 {
            height: 38px;
            width: 214px;
        }
        .auto-style7 {
            width: 214px;
        }
        .auto-style8 {
            width: 102px;
            text-align: right;
            height: 38px;
        }
        .auto-style9 {
            width: 102px;
            text-align: right;
        }
        .auto-style10 {
            height: 38px;
            width: 202px;
        }
        .auto-style11 {
            width: 202px;
        }
        #btnBack{
            height: 18px;
            padding: 8px 12px;
            width: 65px;
            margin-left: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addcompany">
    <table class="auto-style1">
        <tr>
            <td class="auto-style8">Company Name :</td>
            <td class="auto-style5">
                <asp:TextBox ID="txtCompanyName" runat="server" Height="25px" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_CompanyName" runat="server" ControlToValidate="txtCompanyName" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style3">Short Name :</td>
            <td class="auto-style10">
                <asp:TextBox ID="txtComShortName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">Local Name :</td>
            <td class="auto-style5">
                <asp:TextBox ID="txtCompanyLocalName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style3">Local Short Name :</td>
            <td class="auto-style10">
                <asp:TextBox ID="txtCompanyLocalShortName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Website :</td>
            <td class="auto-style7">
                <asp:TextBox ID="txtWebsite" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style2">Email :</td>
            <td class="auto-style11">
                <asp:TextBox ID="txtEmail" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Phone :</td>
            <td class="auto-style7">
                <asp:TextBox ID="txtPhone" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style2">Address :</td>
            <td class="auto-style11">
                <asp:TextBox ID="txtAddress" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Font :</td>
            <td class="auto-style7">
                <asp:DropDownList ID="DropDownListFont" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
            <td class="auto-style2">Font Size :</td>
            <td class="auto-style11">
                <asp:DropDownList ID="DropDownListFontSize" runat="server" Height="30px" Width="205px">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>32</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Art Font :</td>
            <td class="auto-style7">
                <asp:DropDownList ID="DpdArtFont" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
            <td class="auto-style2">Art Font Size :</td>
            <td class="auto-style11">
                <asp:DropDownList ID="DropDownListArtFontSize" runat="server" Height="30px" Width="205px">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>32</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Country :</td>
            <td class="auto-style7">
                <asp:DropDownList ID="DropDownListCountry" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFV_Country" runat="server" ControlToValidate="DropDownListCountry" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style2">Flag :</td>
            <td class="auto-style11">
                <asp:DropDownList ID="DropDownListFlag" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Logo :</td>
            <td class="auto-style7">
                <asp:FileUpload ID="FileUpload_Logo" runat="server" Height="25px" Width="200px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style7">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style11" style="padding: 20px 20px 20px 0px;">
                <a id="btnBack" class="btn btn-default" href="CompanyList.aspx">Back</a>
                <asp:Button ID="btnAddCompany" runat="server" CssClass="btn btn-info btn-addcompany" Text="Add" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>
