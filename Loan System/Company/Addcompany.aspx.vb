﻿Imports System.Web.UI.WebControls.FileUpload
Imports System.Drawing
Imports System.Drawing.Text
Public Class Addcompany
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            addToDropdown("Select CountryCode,CountryName From tblCountry ", DropDownListCountry)
            DropDownListFlag.Items.Add("--Select--")
            DropDownListFlag.Items.Add("Yes")
            DropDownListFlag.Items.Add("No")
            Dim fonts As InstalledFontCollection = New InstalledFontCollection()
            DropDownListFont.Items.Add("--Select--")
            DpdArtFont.Items.Add("--Select--")
            For Each font As FontFamily In fonts.Families
                DropDownListFont.Items.Add(font.Name)
                DpdArtFont.Items.Add(font.Name)
            Next font
        End If


    End Sub

    Protected Sub btnAddCompany_Click(sender As Object, e As EventArgs) Handles btnAddCompany.Click
        Try
            Dim comCode = CheckID("select isnull(Max(comCode),0)+1 from tblCompany")
            RunSQLWithImage("insert into tblCompany([comCode],[comName],[comShortName],[comLocalName],[comLocalShortName],[countryCode],[flag],[email],[phone],[website],[normalFont],[normalSize],[artFont],[artSize],[Address],[Logo] )  " & _
                            " values(" & amStr(comCode) & ",N" & amStr(txtCompanyName.Text) & ",N" & amStr(txtComShortName.Text) & ",N" & amStr(txtCompanyLocalName.Text) &
                          ",N" & amStr(txtCompanyLocalShortName.Text) & ",N" & amStr(DropDownListCountry.SelectedValue()) & "," & amStr(DropDownListFlag.Text) &
                          "," & amStr(txtEmail.Text) & "," & amStr(txtPhone.Text) & "," & amStr(txtWebsite.Text) & "," & amStr(DropDownListFont.SelectedItem.Text) &
                          "," & amStr(DropDownListFontSize.SelectedItem.Text) & "," & amStr(DpdArtFont.SelectedItem.Text) & "," & amStr(DropDownListArtFontSize.SelectedItem.Text) &
                          "," & amStr(txtAddress.Text) & ",@image )", FileUpload_Logo)
            clearForm()
            Response.Redirect("/Company/CompanyList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub clearForm()
        txtCompanyName.Text = ""
        txtEmail.Text = ""
        txtPhone.Text = ""
        txtWebsite.Text = ""
        DropDownListCountry.SelectedIndex = 0

    End Sub

End Class