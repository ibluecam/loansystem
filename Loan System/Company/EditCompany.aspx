﻿<%@ Page Title="Edit Company" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EditCompany.aspx.vb" Inherits="Loan_System.EditCompany" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 70%;
            padding: 10px;
        }
        .auto-style2 {
            width: 111px;
            text-align: right;
        }
        .addcompany {
            background-color: #f5f5f5;
        }
        .btn-addcompany {
            height: 35px !important;
            width: 100px !important;
        }
        .auto-style3 {
            width: 210px;
        }
        .auto-style4 {
            width: 203px;
        }
        .auto-style5 {
            width: 210px;
            text-align: right;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addcompany">
    <table class="auto-style1">
        <tr>
            <td class="auto-style8">Company Name :</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtCompanyName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style3" align="right">Short Name :</td>
            <td class="auto-style10">
                <asp:TextBox ID="txtComShortName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">Local Name :</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtCompanyLocalName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style3" align="right">Local Short Name :</td>
            <td class="auto-style10">
                <asp:TextBox ID="txtCompanyLocalShortName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Website :</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtWebsite" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style5" align="right">Email :</td>
            <td class="auto-style11">
                <asp:TextBox ID="txtEmail" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Phone :</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtPhone" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style5" align="right">Address :</td>
            <td class="auto-style11">
                <asp:TextBox ID="txtAddress" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Font :</td>
            <td class="auto-style4">
                <asp:DropDownList ID="DropDownListFont" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
            <td class="auto-style5" align="right">Font Size :</td>
            <td class="auto-style11">
                <asp:DropDownList ID="DropDownListFontSize" runat="server" Height="30px" Width="205px">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>32</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Art Font :</td>
             <td class="auto-style4">
                <asp:DropDownList ID="DpdArtFont" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
            <td class="auto-style5" align="right">Art Font Size :</td>
            <td class="auto-style11">
                <asp:DropDownList ID="DropDownListArtFontSize" runat="server" Height="30px" Width="205px">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>32</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Country :</td>
            <td class="auto-style4">
                <asp:DropDownList ID="DropDownListCountry" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFV_Country" runat="server" ControlToValidate="DropDownListCountry" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style5" align="right">Flag :</td>
            <td class="auto-style11">
                <asp:DropDownList ID="DropDownListFlag" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Logo :</td>
            <td class="auto-style4">
                <asp:FileUpload ID="FileUpload_Logo" runat="server" Height="25px" Width="200px" />
                <asp:Label ID="lblimage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td style="padding: 20px 20px 20px 0px;" class="auto-style4">
                <asp:Button ID="btnAddCompany" runat="server" CssClass="btn btn-info btn-addcompany" Text="Update" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>