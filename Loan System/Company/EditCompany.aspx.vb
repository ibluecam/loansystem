﻿Imports System.Web
Imports System.IO
Imports System.Drawing.Text
Imports System.Drawing

Public Class EditCompany
    Inherits System.Web.UI.Page
    Dim id As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            id = Request("id")
            DropDownListFlag.Items.Add("--Select--")
            DropDownListFlag.Items.Add("Yes")
            DropDownListFlag.Items.Add("No")
            Dim fonts As InstalledFontCollection = New InstalledFontCollection()
            DropDownListFont.Items.Add("--Select--")
            DpdArtFont.Items.Add("--Select--")
            For Each font As FontFamily In fonts.Families
                DropDownListFont.Items.Add(font.Name)
                DpdArtFont.Items.Add(font.Name)
            Next font
            addToDropdown("Select CountryCode,CountryName From tblCountry ", DropDownListCountry)
            Dim ListCountry = ReadData("SELECT [comCode],[comName],[comShortName],[comLocalName],[comLocalShortName],[countryCode]," &
                                       "[flag],[email],[phone],[website],[normalFont],[normalSize],[artFont],[artSize],[Address],[Logo]  From tblCompany where comCode=" & id)
            txtCompanyName.Text = ListCountry.Item(1)
            txtComShortName.Text = ListCountry.Item(2)
            txtCompanyLocalName.Text = ListCountry.Item(3)
            txtCompanyLocalShortName.Text = ListCountry.Item(4)
            DropDownListCountry.Text = ListCountry.Item(5)
            DropDownListFlag.Text = ListCountry.Item(6)
            txtEmail.Text = ListCountry.Item(7)
            txtPhone.Text = ListCountry.Item(8)
            txtWebsite.Text = ListCountry.Item(9)
            DropDownListFont.Text = ListCountry.Item(10)
            DropDownListFontSize.Text = ListCountry.Item(11)
            DpdArtFont.Text = ListCountry.Item(12)
            DropDownListArtFontSize.Text = ListCountry.Item(13)
            txtAddress.Text = ListCountry.Item(14)
        End If
    End Sub

    Protected Sub btnAddCompany_Click(sender As Object, e As EventArgs) Handles btnAddCompany.Click
        id = Request("id")
        Dim SQL As String
        SQL = "UPDATE tblCompany SET comName=N" & amStr(txtCompanyName.Text) & ",comShortName=N" & amStr(txtComShortName.Text) & ",comLocalName=N" & amStr(txtCompanyLocalName.Text) &
            ",comLocalShortName=N" & amStr(txtCompanyLocalShortName.Text) & ",countryCode=" & amStr(DropDownListCountry.SelectedValue) & ",flag=" & amStr(DropDownListFlag.Text) &
            ",email=" & amStr(txtEmail.Text) & ",phone=" & amStr(txtPhone.Text) & ",website=" & amStr(txtWebsite.Text) & ",normalFont=" & amStr(DropDownListFont.SelectedItem.Text) &
            ",normalSize=" & amStr(DropDownListFontSize.SelectedItem.Text) & ",artFont=" & amStr(DpdArtFont.SelectedItem.Text) & ",artSize=" & amStr(DropDownListArtFontSize.SelectedItem.Text) &
            ",Address=" & amStr(txtAddress.Text) & ""

        If FileUpload_Logo.HasFile Then
            SQL += " ,Logo=@image Where comCode=" & id
            RunSQLWithImage(SQL, FileUpload_Logo)
        Else
            SQL += " Where comCode=" & id
            RunSQL(SQL)
        End If
        Response.Redirect("/Company/CompanyList.aspx", False)
    End Sub
End Class