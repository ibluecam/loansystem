﻿<%@ Page Title="Users / Add User" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Register.aspx.vb" Inherits="Loan_System.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            background-color: #f5f5f5;
            border-radius: 4px;
            padding-top: 10px;
            width: 71%;
        }
        .tdLabel {
            width: 17%;
            text-align: right;
        }
        .tdInput {
            width: 34%;
        }
            .tdInput input {
                width: 200px;
                height: 22px;
            }
            .tdInput select {
                width: 205px;
                height: 27px;
            }
        .tdbnt {
            text-align: center;
        }
        .btn-create {
            height: 35px !important;
            width: 100px !important;
        }
        .adduser {
            background-color: #f5f5f5;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
    <div class="adduser">
        <table class="auto-style1">
            <tr>
                <td class="tdLabel">
                    <asp:Label ID="Label12" runat="server" Font-Bold="False" Font-Size="Small" Text="User Name :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                    <span style="color: red; font-weight: bold; font-size: 20px; padding: 0px 5px;"><asp:RequiredFieldValidator ID="UserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                </td>
                <td class="tdLabel">
                    <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Size="Small" Text="Sex :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:DropDownList ID="Sex" runat="server">
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">
                    <asp:Label ID="Label14" runat="server" Font-Bold="False" Font-Size="Small" Text="Date of Birth :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox CssClass="datepicker" ID="datepickerDOB" runat="server"></asp:TextBox>
                    <span style="color: red; font-weight: bold; font-size: 20px; padding: 0px 5px;"><asp:RequiredFieldValidator ID="DateofBirth" runat="server" ControlToValidate="datepickerDOB" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                </td>
                <td class="tdLabel">
                    <asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Size="Small" Text="Phone :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox ID="txtPhone" runat="server" TextMode="Phone"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">
                    <asp:Label ID="Label15" runat="server" Font-Bold="False" Font-Size="Small" Text="Desigantion :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox ID="txtDesigantion" runat="server"></asp:TextBox>
                </td>
                <td class="tdLabel">
                    <asp:Label ID="Label9" runat="server" Font-Bold="False" Font-Size="Small" Text="Email :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox ID="txtEmail" runat="server" TextMode="Email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">
                    <asp:Label ID="Label5" runat="server" Font-Bold="False" Font-Size="Small" Text="Effective Date :"></asp:Label>
                </td>
                <td class="tdInput"><asp:TextBox CssClass="datepicker" ID="datepickerPWChangeDate" runat="server"></asp:TextBox></td>
                <td class="tdLabel">
                    <asp:Label ID="Label16" runat="server" Font-Bold="False" Font-Size="Small" Text="Expiry Date :"></asp:Label>
                </td>
                <td class="tdInput"><asp:TextBox CssClass="datepicker" ID="datepickerPWExpiryDate" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">
                    <asp:Label ID="Label6" runat="server" Font-Bold="False" Font-Size="Small" Text="Position :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox ID="txtPosition" runat="server"></asp:TextBox>
                </td>
                <td class="tdLabel">
                    <asp:Label ID="Label17" runat="server" Font-Bold="False" Font-Size="Small" Text="Group :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">
                    <asp:Label ID="Label7" runat="server" Font-Bold="False" Font-Size="Small" Text="Role  :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:DropDownList ID="RoleID" runat="server">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="tdLabel">
                    <asp:Label ID="Label18" runat="server" Font-Bold="False" Font-Size="Small" Text="Branch :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:DropDownList ID="BranchID" runat="server">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">
                    <asp:Label ID="Label8" runat="server" Font-Bold="False" Font-Size="Small" Text="Password :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <span style="color: red; font-weight: bold; font-size: 20px; padding: 0px 5px;"><asp:RequiredFieldValidator ID="Password" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                </td>
                <td class="tdLabel">
                    <asp:Label ID="Label19" runat="server" Font-Bold="False" Font-Size="Small" Text="Confirm Password :"></asp:Label>
                </td>
                <td class="tdInput">
                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <span style="color: red; font-weight: bold; font-size: 20px; padding: 0px 5px;"><asp:RequiredFieldValidator ID="ConfirmPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="*"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">
                    &nbsp;</td>
                <td class="tdInput" style="padding: 20px 20px 20px 0px;">                
                    <asp:Button ID="btn_Create" runat="server" Text="Add" CssClass="btn btn-info btn-create" />                
                </td>
                <td class="tdLabel">
                    &nbsp;</td>
                <td class="tdInput">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
