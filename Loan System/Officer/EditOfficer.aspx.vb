﻿Public Class EditOfficer
    Inherits System.Web.UI.Page
    Dim id As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            id = Request("id")
            addToDropdown("Select locCode,locName From tblLocation", DpdLocation)
            Dim Officer = ReadData("SELECT [OfficerCode],[FirstName],[LastName],[localFirstName],[localLastName],[DisplayName], " &
                                   "[Gender],[locCode],[title],[CivilStatus] ,convert(NVARCHAR, [BirthDate], 103) AS [BirthDate],[NID]," &
                                   "[Passport],[ResidentBook],[mobile1],[fax],[email] FROM tblOfficer Where OfficerCode= " & id)

            txtFirstName.Text = Officer.Item(1)
            txtLastName.Text = Officer.Item(2)
            txtLocalFirstName.Text = Officer.Item(3)
            txtLocalLastName.Text = Officer.Item(4)
            txtDisplayName.Text = Officer.Item(5)
            DpdGender.Text = Officer.Item(6)
            DpdLocation.Text = Officer.Item(7)
            txtTitle.Text = Officer.Item(8)
            txtCivilStatus.Text = Officer(9)
            txtBirthDate.Text = Officer.Item(10)
            txtNID.Text = Officer.Item(11)
            txtPassport.Text = Officer.Item(12)
            txtResidentBook.Text = Officer.Item(13)
            txtMobile.Text = Officer.Item(14)
            txtFax.Text = Officer.Item(15)
            txtEmail.Text = Officer.Item(16)
        End If
    End Sub

    Protected Sub btnEditOfficer_Click(sender As Object, e As EventArgs) Handles btnEditOfficer.Click
        Try
            id = Request("id")
            RunSQL("UPDATE tblOfficer SET FirstName=N" & amStr(txtFirstName.Text) & ",LastName=N" & amStr(txtLastName.Text) & ",localFirstName=N" & amStr(txtLocalFirstName.Text) &
                   ",localLastName=N" & amStr(txtLocalLastName.Text) & ",DisplayName=" & amStr(txtDisplayName.Text) & ",Gender=" & amStr(DpdGender.SelectedItem.Text) &
                   ",locCode=" & amStr(DpdLocation.SelectedValue) & ",title=N" & amStr(txtTitle.Text) & ",CivilStatus=N" & amStr(txtCivilStatus.Text) & ",BirthDate=N" & amStr(txtBirthDate.Text) &
                   ",NID=N" & amStr(txtNID.Text) & ",Passport=N" & amStr(txtPassport.Text) & ",ResidentBook=N" & amStr(txtResidentBook.Text) & ",mobile1=" & amStr(txtMobile.Text) & ",fax=N" & amStr(txtFax.Text) &
                   ",email=" & amStr(txtEmail.Text) & " Where OfficerCode=" & id)
            Response.Redirect("/Officer/OfficerList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class