﻿Public Class AddOfficer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            addToDropdown("Select locCode,locName From tblLocation", DpdLocation)
        End If
    End Sub

    Protected Sub btnAddBranch_Click(sender As Object, e As EventArgs) Handles btnAddBranch.Click
        Try
            Dim OfficeCode = CheckID("select isnull(Max(OfficerCode),0)+1 from tblOfficer")
            RunSQL(" INSERT INTO tblOfficer([OfficerCode],[FirstName],[LastName],[localFirstName],[localLastName],[DisplayName],[Gender],[locCode],[title],[CivilStatus] ,[BirthDate],[NID],[Passport],[ResidentBook],[mobile1],[fax],[email]) " & _
                   " Values(" & amStr(OfficeCode) & ",N" & amStr(txtFirstName.Text) & ",N" & amStr(txtLastName.Text) & ",N" & amStr(txtLocalFirstName.Text) &
                   ",N" & amStr(txtLocalLastName.Text) & ",N" & amStr(txtDisplayName.Text) & ",N" & amStr(DpdGender.SelectedItem.Text) &
                   "," & amStr(DpdLocation.SelectedValue) & ",N" & amStr(txtTitle.Text) & ",N" & amStr(txtCivilStatus.Text) & "," & amStr(txtBirthDate.Text) &
                   "," & amStr(txtNID.Text) & "," & amStr(txtPassport.Text) & "," & amStr(txtResidentBook.Text) & "," & amStr(txtMobile.Text) &
                   "," & amStr(txtFax.Text) & "," & amStr(txtEmail.Text) & ")")
            Response.Redirect("/Officer/OfficerList.aspx", False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class