﻿<%@ Page Title="Add Officer" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddOfficer.aspx.vb" Inherits="Loan_System.AddOfficer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .addofficer {
            background-color: #f5f5f5;
        }
        .auto-style1 {
            padding: 10px;
            width: 100%;
        }
        .auto-style3 {
            width: 222px;
        }
        .auto-style4 {
            width: 120px;
        }
        #btnBack{
            height: 18px;
            padding: 8px 12px;
            width: 65px;
            margin-left: 3px;
        }
        .btn-addbranch {
            height: 35px !important;
            margin-left: 28px;
            width: 80px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-fw fa-flag"></i>  <%: Title %>
        </li>
    </ol>
<div class="addofficer">
    <table class="auto-style1">
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelFirstName" runat="server" Text="First Name :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtFirstName" runat="server" Height="25px" style="margin-left: 0px" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelLastName" runat="server" Text="Last Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLastName" runat="server" Height="25px" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelLocalFirstName" runat="server" Text="Local First Name :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtLocalFirstName" runat="server" Height="25px" style="margin-left: 0px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelLocalLastName" runat="server" Text="Local Last Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLocalLastName" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelDisplayName" runat="server" Text="Display Name :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtDisplayName" runat="server" Height="25px" style="margin-left: 0px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelTitle" runat="server" Text="Title :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTitle" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelGender" runat="server" Text="Gender :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:DropDownList ID="DpdGender" runat="server" Height="30px" Width="205px">
                    <asp:ListItem>Male</asp:ListItem>
                    <asp:ListItem>Female</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelLocCode" runat="server" Text="Location :"></asp:Label>
            </td>
            <td>
               <asp:DropDownList ID="DpdLocation" runat="server" Height="30px" Width="205px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelBirthDate" runat="server" Text="Date of Birth :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox CssClass="datepicker" ID="txtBirthDate" Height="25px" style="margin-left: 0px" Width="200px" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelNID" runat="server" Text="NID :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNID" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelCivilStatus" runat="server" Text="Civil Status :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtCivilStatus" runat="server" Height="25px" style="margin-left: 0px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelPassport" runat="server" Text="Passport :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPassport" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelResidentBook" runat="server" Text="Resident Book :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtResidentBook" runat="server" Height="25px" style="margin-left: 0px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelMobile1" runat="server" Text="Mobile :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtMobile" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelEmail" runat="server" Text="Email :"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtEmail" runat="server" Height="25px" style="margin-left: 0px" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style4" align="right">
                <asp:Label ID="LabelFax" runat="server" Text="Fax :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtFax" runat="server" Height="25px" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style4" align="right">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4" align="right">&nbsp;</td>
            <td style="padding: 20px 20px 20px 0px;">
                <a id="btnBack" class="btn btn-default" href="OfficerList.aspx">Back</a>
                <asp:Button ID="btnAddBranch" runat="server" CssClass="btn btn-info btn-addbranch" Text="Add" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>
