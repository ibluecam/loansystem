﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports System.Web.Security
Public Class Login
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        makeConnection()
        If Not IsPostBack Then
            Dim userName = Request.Cookies("UserName")
            Dim pwd = Request.Cookies("Password")
            Dim remember = Request.Cookies("RememberMe")
            If ((Not (userName) Is Nothing) AndAlso (Not (pwd) Is Nothing) AndAlso (Not (remember) Is Nothing)) Then
                RememberMe.Checked = True
                txtUserName.Text = Request.Cookies("UserName").Value
                txtPassword.Attributes("value") = Request.Cookies("Password").Value
            End If
        End If
    End Sub
    Protected Sub btnLogIn_Click(sender As Object, e As EventArgs) Handles btnLogIn.Click
        Dim userId As Integer = 0
        Dim b As Boolean
        b = False
        Using cmd As New SqlCommand("Validate_User")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Username", txtUserName.Text)
            cmd.Parameters.AddWithValue("@Password", txtPassword.Text)
            cmd.Connection = CN
            userId = Convert.ToInt32(cmd.ExecuteScalar())
        End Using
        Select Case userId
            Case -1
                tblStatus.Text = "Username and/or password is incorrect."
                Exit Select
            Case -2
                tblStatus.Text = "Account has not been activated."
                Exit Select
            Case Else
                If RememberMe.Checked Then
                    Response.Cookies("UserName").Expires = DateTime.Now.AddDays(30)
                    Response.Cookies("Password").Expires = DateTime.Now.AddDays(30)
                    Response.Cookies("RememberMe").Value = 1
                Else
                    Response.Cookies("UserName").Expires = DateTime.Now.AddDays(-1)
                    Response.Cookies("Password").Expires = DateTime.Now.AddDays(-1)
                End If
                Response.Cookies("UserName").Value = txtUserName.Text.Trim
                Response.Cookies("Password").Value = txtPassword.Text.Trim
                FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, False)
                Exit Select
        End Select
    End Sub
End Class